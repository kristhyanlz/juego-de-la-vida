# Author: Kurt Zubia
# Basado en la implementación de DOT CSV


import pygame
import numpy as np
import time

pygame.init()

width, height = 650, 650
screen = pygame.display.set_mode((height, width))

n_casilleros = (50,50)#Dimensiones de la cuadricula
gameState = np.zeros(n_casilleros)

### Read ini_state
with open("ini_state.txt", "r") as f_ini:
  for i, line in enumerate(f_ini):#Leemos cada linea del fichero
    line = line.split()#Ponemos cada elemento en un arreglo, omitiendo espacios y saltos de linea
    for j, idx in enumerate(line):#Recorremos el arreglo
      if idx == "1":#Celula viva
        gameState[i][j] = 1#Actualizamos el gameState

nxC, nyC = len(gameState[0]), len(gameState)
dimCW = width/nxC
dimCH = height/nyC

run = True
while run:
  pygame.event.pump()
  for event in pygame.event.get():
        if event.type == pygame.QUIT:
            run = False
  newGameState = np.copy(gameState)
  screen.fill((25,25,25))
  for y in range(nxC):
    for x in range(nyC):
      #Calcula el numero de vecinos vivos
      n_heigh = gameState[(x-1) % nxC, (y-1) % nyC] + \
                gameState[(x)   % nxC, (y-1) % nyC] + \
                gameState[(x+1) % nxC, (y-1) % nyC] + \
                gameState[(x-1) % nxC, (y)   % nyC] + \
                gameState[(x+1) % nxC, (y)   % nyC] + \
                gameState[(x-1) % nxC, (y+1) % nyC] + \
                gameState[(x)   % nxC, (y+1) % nyC] + \
                gameState[(x+1) % nxC, (y+1) % nyC]
      #Regla 1: Revivir célula si exactamente 3 vecinos están vivos
      if gameState[x, y] == 0 and n_heigh == 3:
        newGameState[x, y] = 1

      #Regla 2: Muere la célula si tiene menos de 2 o más de 3 vecinas vivas
      elif gameState[x, y] == 1 and (n_heigh < 2 or n_heigh > 3):
        newGameState[x, y] = 0
      #Poligono de cada celda
      poly = [((x)*dimCW, y*dimCH),
              ((x+1)*dimCW, y*dimCH),
              ((x+1)*dimCW, (y+1)*dimCH),
              ((x)*dimCW, (y+1)*dimCH)]

      if newGameState[x,y] == 0:
        pygame.draw.polygon(screen, (128,128,128), poly, 1)# 1 = Linea, int
      else:
        pygame.draw.polygon(screen, (255,255,255), poly, 0)#0 = Fill
  
  #Actualizar el estado
  gameState = np.copy(newGameState)
  
  pygame.display.flip()
  time.sleep(0.05)